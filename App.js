import 'react-native-gesture-handler';
import React from 'react';
import { Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Provider } from 'react-redux';
import Router from './src/router';
import store from './src/store';

let {width} = Dimensions.get('window');
EStyleSheet.build({
	$rem: width / 380
});

function App() {
	return (
		<Provider store={store}>
			<Router />
		</Provider>
	);
}

export default App;