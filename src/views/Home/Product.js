import React, { Fragment, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { View, Text, TextInput, Image, FlatList, TouchableOpacity, ActivityIndicator, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import { ProductCarousel, ProductNavbar, ProductCard, Toast } from '../../components';
import { Color, Font, Size } from '../../assets/styles';
import { get, post } from '../../utils/Request';
import { numberFormat } from '../../utils/Helpers';
import { storeToast } from '../../store/toast';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

function Product ({ route, navigation }) {
    const { productId } = route.params;
	const [product, setProduct] = useState({});
	const [productRecommendation, setProductRecommendation] = useState([]);
	const [productImages, setProductImages] = useState([]);
	const [transparent, setTransparent] = useState(true);
	const [loader, setLoader] = useState(false);
	const [recommendationLoader, setRecommendationLoader] = useState(false);
	const [quantity, setQuantity] = useState('1');
	const [disableAddToCart, setDisableAddToCart] = useState(false);
	const cartIcon = require('../../assets/icons/cart.png');
    const user = useSelector(state => state.user);
    const toast = useSelector(state => state.toast);
    const dispatch = useDispatch();

    useEffect(() => {
    	getProduct();
    	getProductRecommendation();
    }, [productId])

	const getProduct = async () => {
		setLoader(true);

		let url = '/product/detail/' + productId;
		const response = await get(url);
		if (response) {
			if (response.status === 'success') {
				setProduct(response.data);
				setProductImages(response.data.images);
			}
			setLoader(false);
		}
	}

	const getProductRecommendation = async () => {
		setRecommendationLoader(true);

		let url = '/product/detail/recommendation/' + productId;
		const response = await get(url);
		if (response) {
			if (response.status === 'success') {
				setProductRecommendation(response.data);
			}
			setRecommendationLoader(false);
		}
	}

	const detailProduct = (productId) => {
		navigation.navigate('Product', {
            productId: productId,
        });
	}
	
	const incrementQty = () => {
		let qty = parseInt(quantity) + 1;
		if (product.stock && qty > product.stock) {
			qty = product.stock;
		}

		setQuantity(qty.toString());
	}

	const decrementQty = () => {
		let qty = parseInt(quantity) - 1;

		if (qty > 0) {
			setQuantity(qty.toString());
		}
	}

	const onChangeQty = (value) => {
		// remove all chars except number
		value = value.replace(/\D/g, '');

		// if value is empty
		if (!value) {
			value = 0;
		}

		setQuantity(value);
	}

	const addToCart = async () => {
		if (!user) {
			return navigation.navigate('Profile', {
				prevScreen: 'Product'
			});
		}

		if (quantity && !isNaN(quantity)) {
			let qty = parseInt(quantity);
			let stock = parseInt(product.stock);

			if (qty > 0 && qty <= stock) {
				setDisableAddToCart(true);

				let params = {
					product_id: product.id,
					quantity: quantity
				}
				const response = await post('/cart/add', params);
				if (response) {
					let _toast = {
						show: true,
						type: 'success',
						message: response.messages.join("\n")
					};
					if (response.status !== 'success') {
						_toast.type = 'error';
					}
					dispatch(storeToast(_toast));
					setDisableAddToCart(false);
				}
			}
		}
	}

	const share = () => {

	}

	return (
		<Fragment>
			<ParallaxScrollView
				backgroundColor="#fff"
				parallaxHeaderHeight={screenWidth}
				renderForeground={() => !loader ? (
					<ProductCarousel
						sliderWidth={screenWidth}
						itemWidth={screenWidth}
						images={productImages}
						onShare={() => share()}
					/>
				) : null}
				renderFixedHeader={() => !loader ? (
					<ProductNavbar
						transparent={transparent}
						productId={productId}
						wishlist={product.wishlist}
						onBack={() => navigation.goBack()}
					/>
				) : null}
				scrollEvent={(e) => {
					if (e.nativeEvent.contentOffset.y >= (screenWidth - 70)) {
						setTransparent(false);
					}
					else {
						setTransparent(true);
					}
				}}
			>

				<View style={_styles.screen}>
					<View style={_styles.content}>
						{!loader &&
							<Fragment>
								<Text style={_styles.name}>{product.name}</Text>

								<View style={_styles.rowSpaceBetween}>
									<Text style={_styles.price}>{numberFormat(product.price, 'Rp')}</Text>
									<View style={_styles.row}>
										<View style={_styles.tag}>
											<Text style={_styles.tagText}>Stock: {numberFormat(product.stock)}</Text>
										</View>
										<View style={_styles.tag}>
											<Text style={_styles.tagText}>Weight: {numberFormat(product.weight)}</Text>
										</View>
									</View>
								</View>

								<Text style={_styles.code}>SKU: {product.code}</Text>

								<Text style={_styles.description}>{product.description}</Text>
							</Fragment>
						}

						<Text style={_styles.recommendation}>Product Recommendation</Text>

						{recommendationLoader ?
							<ActivityIndicator size="large" style={_styles.loader} color={Color.primary} />
							:
							<FlatList
								contentContainerStyle={_styles.flatlist}
								columnWrapperStyle={_styles.flatlistColumn}
								numColumns={2}
								horizontal={false}
								data={productRecommendation}
								renderItem={({ item }) => (
									<ProductCard
										id={item.id}
										name={item.name}
										image={item.image}
										price={item.price}
										wishlist={item.wishlist}
										onPress={() => detailProduct(item.id)}
									/>
								)}
								keyExtractor={(item, index) => item.id}
							/>
						}

					</View>
				</View>
			</ParallaxScrollView>

			<Toast show={toast.show} type={toast.type} message={toast.message} />

			{loader ?
				<ActivityIndicator size="large" style={_styles.loader} color={Color.primary} />
				:
				<View style={_styles.addToCart}>
					<View></View>
					<View style={_styles.row}>
						<View style={_styles.row}>
							<TouchableOpacity style={[_styles.qtyButton, _styles.minusButton]}
								activeOpacity={0.5}
								onPress={() => decrementQty()}
							>
								<Text style={_styles.qtyButtonText}>-</Text>
							</TouchableOpacity>
							<TextInput
								style={_styles.input}
								textAlign="center"
								keyboardType="number-pad"
								returnKeyType="done"
								value={quantity}
								onChangeText={(value) => onChangeQty(value)}
							/>
							<TouchableOpacity style={[_styles.qtyButton, _styles.plusButton]}
								activeOpacity={0.5}
								onPress={() => incrementQty()}
							>
								<Text style={_styles.qtyButtonText}>+</Text>
							</TouchableOpacity>
						</View>

						<TouchableOpacity
							style={_styles.buttonCart}
							activeOpacity={0.5}
							onPress={addToCart}
						>
							<Image style={_styles.iconCart} source={cartIcon} resizeMode="center" />
						</TouchableOpacity>
					</View>
				</View>
			}
		</Fragment>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		flex: 1
	},
	content: {
		paddingVertical: '16rem',
		paddingHorizontal: '10rem'
	},
	name: {
		fontFamily: Font.bold,
		fontSize: Size.fontTitle + 'rem',
	},
	rowSpaceBetween: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	price: {
		fontFamily: Font.bold,
		fontSize: Size.fontTitle + 'rem',
		marginTop: '10rem'
	},
	row: {
		flexDirection: 'row'
	},
	tag: {
		minWidth: '50rem',
		height: '24rem',
		borderRadius: '16rem',
		borderWidth: 1,
		borderColor: Color.regular,
		paddingHorizontal: '12rem',
		paddingTop: '5rem',
		paddingBottom: '3rem',
		marginTop: '3rem',
		marginLeft: '8rem',
	},
	tagText: {
		textAlign: 'center',
		textAlignVertical: 'center',
		fontSize: Size.fontSmall + 'rem',
	},
	code: {
		marginTop: '16rem',
	},
	description: {
		marginTop: '10rem',
	},
	recommendation: {
		marginTop: '20rem',
		fontFamily: Font.bold,
		fontSize: Size.fontTitle + 'rem',
	},
	flatlist: {
		marginTop: '10rem',
	},
	flatlistColumn: {
		justifyContent: 'space-between',
		marginBottom: '14rem'
	},
	addToCart: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: '#fff',
		paddingVertical: '10rem',
		paddingHorizontal: '16rem',
		borderTopLeftRadius: '16rem',
		borderTopRightRadius: '16rem',
		borderTopWidth: 1,
		borderLeftWidth: 1,
		borderRightWidth: 1,
		borderColor: Color.grey,
		position: 'absolute',
		bottom: 0,
		zIndex: 10,
	},
	plusButton: {
		borderTopRightRadius: '8rem',
		borderBottomRightRadius: '8rem'
	},
	minusButton: {
		borderTopLeftRadius: '8rem',
		borderBottomLeftRadius: '8rem'
	},
	qtyButton: {
		width: '45rem',
		height: Size.buttonIconHeight + 'rem',
		alignItems: 'center',
		justifyContent: 'center',
		borderWidth: 1,
		borderColor: Color.grey
	},
	qtyButtonText: {
		fontFamily: Font.bold,
		fontSize: '24rem',
		color: Color.regular
	},
	input: {
		width: '55rem',
		height: Size.buttonIconHeight + 'rem',
		fontFamily: Font.regular,
		fontSize: '16rem',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderColor: Color.grey,
		padding: 0
	},
	buttonCart: {
		height: Size.buttonIconHeight + 'rem',
		paddingHorizontal: '12rem',
		alignItems: 'center',
		justifyContent: 'center',
		marginLeft: '16rem',
		borderWidth: 1,
		borderColor: Color.grey,
		borderRadius: '8rem'
	},
	iconCart: {
		width: '20rem',
		height: '20rem'
	},
	loader: {
		backgroundColor: '#fff',
		borderRadius: '22rem',
		padding: '4rem',
		position: 'absolute',
		top: screenHeight/4 + 'rem',
		zIndex: 10,
		alignSelf: 'center'
	}
});

export default Product;