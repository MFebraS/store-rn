import React, { Fragment, useEffect, useState, useContext, useCallback } from 'react';
import { View, Text, FlatList, RefreshControl, ActivityIndicator, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import { CustomStatusBar, HomeHeader, HomeNavbar, ProductCard } from '../../components';
import { SearchContext } from '../../contexts';
import { get, post } from '../../utils/Request';
import { Color } from '../../assets/styles';

const { height: screenHeight } = Dimensions.get('window');
const isIphoneX = screenHeight === 812 || screenHeight === 896;
const navbarHeight = Platform.OS === 'ios' ? (isIphoneX ? 88 : 64) : 64;

function Home ({ navigation }) {
	const [categories, setCategories] = useState([]);
	const [products, setProducts] = useState([]);
	const [category, setCategory] = useState('');
	const [brand, setBrand] = useState(true);
	const [loader, setLoader] = useState(false);
	const [afterLoad, setAfterLoad] = useState(false);
	const [refreshing, setRefreshing] = useState(false);
	const searchContext = useContext(SearchContext);

    useEffect(() => {
    	getCategories();
    }, [])

    useEffect(() => {
    	getProducts();
    }, [category])

    useEffect(() => {
    	if (searchContext.search) {
	    	getProducts();
	    }
    }, [searchContext.search])

	const getCategories = async () => {
		setLoader(true);
		
		const response = await get('/category');
		if (response.status === 'success') {
			setCategories(response.data);

			setLoader(false);
		}
		else {
			setLoader(false);
		}
	}

	const getProducts = async () => {
		setLoader(true);
		setAfterLoad(false);
		searchContext.setSearch(false);

		let params = {
			search 		: searchContext.keyword,
			category_id : category,
		}
		const response = await post('/product', params);
		if (response) {
			if (response.status === 'success') {
				setProducts(response.data);
			}
			setLoader(false);
			setAfterLoad(true);
		}
	}

    const onRefresh = useCallback(() => {
    	setRefreshing(true);
    	setProducts([]);
    	getProducts();
    	setRefreshing(false);
    }, [loader])

	const detailProduct = (productId) => {
		navigation.navigate('Product', {
            productId: productId,
        });
	}

	return (
		<Fragment>
			<CustomStatusBar />
			<ParallaxScrollView
				backgroundColor={Color.primary}
				parallaxHeaderHeight={174}
				renderForeground={() => (
					<HomeHeader
						data={categories}
						onLabelPress={(e) => setCategory(e)}
						activeLabel={category}
					/>
				)}
				stickyHeaderHeight={navbarHeight}
				renderFixedHeader={() => (
					<HomeNavbar brand={brand} />
				)}
				scrollEvent={(e) => {
					if (e.nativeEvent.contentOffset.y >= 50) {
						setBrand(false);
					}
					else {
						setBrand(true);
					}
				}}
				refreshControl={
		        	<RefreshControl colors={[Color.primary]} refreshing={refreshing} onRefresh={onRefresh} />
		        }
			>

				<View style={_styles.screen}>
					<View style={_styles.topEdge} />

					<View style={_styles.content}>
						{loader &&
							<ActivityIndicator size="large" style={_styles.loader} color={Color.primary} />
						}

						<FlatList
							contentContainerStyle={_styles.flatlist}
							columnWrapperStyle={_styles.flatlistColumn}
							numColumns={2}
							horizontal={false}
							data={products}
							renderItem={({ item }) => (
								<ProductCard
									id={item.id}
									name={item.name}
									image={item.image}
									price={item.price}
									wishlist={item.wishlist}
									onPress={() => detailProduct(item.id)}
								/>
							)}
							keyExtractor={(item, index) => item.id}
							ListEmptyComponent={() => (afterLoad ? <Text style={_styles.noData}>No Product</Text> : null)}
						/>
					</View>
				</View>

			</ParallaxScrollView>
		</Fragment>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		flex: 1
	},
	topEdge: {
		height: '36rem',
		borderBottomRightRadius: '36rem',
		borderBottomLeftRadius: '36rem',
		backgroundColor: Color.primary
	},
	content: {
		flex: 1,
	},
	flatlist: {
		paddingTop: '14rem',
		paddingHorizontal: '12rem'
	},
	flatlistColumn: {
		justifyContent: 'space-between',
		marginBottom: '14rem'
	},
	noData: {
		textAlign: 'center',
		fontStyle: 'italic',
		color: Color.grey,
		marginTop: '16rem'
	},
	loader: {
		backgroundColor: '#fff',
		borderRadius: '22rem',
		padding: '4rem',
		position: 'absolute',
		top: '30rem',
		zIndex: 10,
		alignSelf: 'center'
	}
});

export default Home;