import React, { useEffect, useState, useRef } from 'react';
import { View, Text, Alert } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useDispatch } from 'react-redux';
import { getDeviceName } from 'react-native-device-info';
import { post } from '../../utils/Request';
import { storeUser } from '../../store/user';
import { storeToken } from '../../store/token';
import { showLoader, hideLoader } from '../../store/loader';
import { Screen, InputIcon, Button } from '../../components';
import { Color, Font } from '../../assets/styles';

function Register ({ navigation }) {
	const [name, setName] = useState();
	const [email, setEmail] = useState();
	const [phone, setPhone] = useState();
	const [password, setPassword] = useState();
	const [passwordConfirmation, setPasswordConfirmation] = useState();
	const [disabled, setDisabled] = useState(true);
    const emailRef = useRef(null);
    const phoneRef = useRef(null);
    const passwordRef = useRef(null);
    const passwordConfirmationRef = useRef(null);
    const dispatch = useDispatch();

    useEffect(() => {
    	let _disabled = true;
    	if (name && email && phone &&
    		password && password.length === 6 && password === passwordConfirmation) {
    		_disabled = false;
    	}
    	setDisabled(_disabled);
    }, [name, email, phone, password, passwordConfirmation])

    const focusEmail = () => {
        emailRef.current.focus();
    }

    const focusPhone = () => {
        phoneRef.current.focus();
    }

    const focusPassword = () => {
        passwordRef.current.focus();
    }

    const focusPasswordConfirmation = () => {
        passwordConfirmationRef.current.focus();
    }

	const register = () => {
		if (!disabled) {
			dispatch(showLoader());

			getDeviceName().then(async (device_name) => {
				let params = {
					device_name : device_name,
					name 		: name,
					email 		: email,
					phone 		: phone,
					password 	: password,
					password_confirmation: passwordConfirmation
				}
				const response = await post('/auth/register', params);
				if (response.status === 'success') {
					setName('');
					setEmail('');
					setPhone('');
					setPassword('');
					setPasswordConfirmation('');

					let user = response.data.user;

					dispatch(hideLoader());
					dispatch(storeUser(user));
					dispatch(storeToken(response.data.token));
				}
				else {
					dispatch(hideLoader());

					let messages = response.messages.join("\n");
					Alert.alert('Oops', messages);
				}
			});
		}
	}

	const login = () => {
		navigation.navigate('Login');
	}

	return (
		<Screen contentContainerStyle={_styles.screen}>
			<View style={_styles.container}>
				<View style={_styles.header}>
					<Text style={_styles.title}>Sign Up</Text>
				</View>

				<View style={_styles.form}>
					<InputIcon
						icon={require('../../assets/icons/user.png')}
						placeholder="Name"
						keyboardType="default"
						returnKeyType="next"
						value={name}
						onChangeText={(value) => setName(value)}
						blurOnSubmit={false}
						onSubmitEditing={focusEmail}
					/>
					<InputIcon
						ref={emailRef}
						style={_styles.input}
						icon={require('../../assets/icons/mail.png')}
						placeholder="Email"
						keyboardType="email-address"
						autoCompleteType="email"
						autoCapitalize="none"
						returnKeyType="next"
						value={email}
						onChangeText={(value) => setEmail(value)}
						blurOnSubmit={false}
						onSubmitEditing={focusPhone}
					/>
					<InputIcon
						ref={phoneRef}
						style={_styles.input}
						icon={require('../../assets/icons/phone.png')}
						placeholder="Phone"
						keyboardType="numeric"
						returnKeyType="next"
						value={phone}
						onChangeText={(value) => setPhone(value)}
						blurOnSubmit={false}
						onSubmitEditing={focusPassword}
					/>
					<InputIcon
						ref={passwordRef}
						style={_styles.input}
						icon={require('../../assets/icons/lock.png')}
						type="password"
						placeholder="Password"
						maxLength={6}
						keyboardType="default"
						autoCompleteType="off"
						autoCapitalize="none"
						returnKeyType="next"
						value={password}
						onChangeText={(value) => setPassword(value)}
						blurOnSubmit={false}
						onSubmitEditing={focusPasswordConfirmation}
					/>
					<InputIcon
						ref={passwordConfirmationRef}
						style={_styles.input}
						icon={require('../../assets/icons/lock.png')}
						type="password"
						placeholder="Re-type Password"
						maxLength={6}
						keyboardType="default"
						autoCompleteType="off"
						autoCapitalize="none"
						returnKeyType="send"
						value={passwordConfirmation}
						onChangeText={(value) => setPasswordConfirmation(value)}
						blurOnSubmit={false}
						onSubmitEditing={register}
					/>

					<Button
						style={_styles.button}
						text="Sign Up"
						onPress={register}
						disabled={disabled}
					/>
				</View>

				<View style={_styles.footer}>
					<Text>Already have an account?</Text>
					<Text
						style={_styles.login}
						onPress={login}
					>
						Login
					</Text>
				</View>
			</View>
		</Screen>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		flexGrow: 1
	},
	container: {
		flex: 1,
		flexDirection: 'column',
		paddingVertical: '16rem'
	},
	header: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	title: {
		fontFamily: Font.bold,
		fontSize: '24rem'
	},
	form: {
		paddingHorizontal: '22rem',
		marginVertical: '22rem'
	},
	input: {
		marginTop: '16rem'
	},
	button: {
		marginTop: '32rem'
	},
	footer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'flex-end'
	},
	login: {
		color: Color.primary,
		marginLeft: '5rem'
	}
});

export default Register;