import React, { useEffect, useState, useRef } from 'react';
import { View, Text, Alert } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useDispatch } from 'react-redux';
import { getDeviceName } from 'react-native-device-info';
import { post } from '../../utils/Request';
import { storeUser } from '../../store/user';
import { storeToken } from '../../store/token';
import { showLoader, hideLoader } from '../../store/loader';
import { Screen, InputIcon, Button } from '../../components';
import { Color, Font } from '../../assets/styles';

function Login ({ navigation }) {
	const [email, setEmail] = useState();
	const [password, setPassword] = useState();
	const [disabled, setDisabled] = useState(true);
    const passwordRef = useRef(null);
    const dispatch = useDispatch();

    useEffect(() => {
    	let _disabled = true;
    	if (email && password && password.length === 6) {
    		_disabled = false;
    	}
    	setDisabled(_disabled);
    }, [email, password])

    const focusPassword = () => {
        passwordRef.current.focus();
    }

	const login = () => {
		if (!disabled) {
			dispatch(showLoader());

			getDeviceName().then(async (device_name) => {
				let params = {
					email 		: email,
					password 	: password,
					device_name : device_name
				}
				const response = await post('/auth/login', params);
				if (response.status === 'success') {
					setEmail('');
					setPassword('');

					let user = response.data.user;

					dispatch(hideLoader());
					dispatch(storeUser(user));
					dispatch(storeToken(response.data.token));
				}
				else {
					dispatch(hideLoader());

					let messages = response.messages.join("\n");
					Alert.alert('Oops', messages);
				}
			});
		}
	}

	const forgetPassword = () => {
		alert('Forget Password');
	}

	const register = () => {
		navigation.navigate('Register');
	}

	return (
		<Screen contentContainerStyle={_styles.screen}>
			<View style={_styles.container}>
				<View style={_styles.header}>
					<Text style={_styles.title}>Login</Text>
				</View>

				<View style={_styles.form}>
					<InputIcon
						icon={require('../../assets/icons/mail.png')}
						placeholder="Email"
						keyboardType="email-address"
						autoCompleteType="email"
						autoCapitalize="none"
						returnKeyType="next"
						value={email}
						onChangeText={(value) => setEmail(value)}
						blurOnSubmit={false}
						onSubmitEditing={focusPassword}
					/>
					<InputIcon
						ref={passwordRef}
						style={_styles.inputPassword}
						icon={require('../../assets/icons/lock.png')}
						type="password"
						placeholder="Password"
						maxLength={6}
						keyboardType="default"
						autoCompleteType="off"
						autoCapitalize="none"
						returnKeyType="send"
						value={password}
						onChangeText={(value) => setPassword(value)}
						blurOnSubmit={false}
						onSubmitEditing={login}
					/>

					<Button
						style={_styles.button}
						text="Login"
						onPress={login}
						disabled={disabled}
					/>

					<Text
						style={_styles.forgetPassword}
						onPress={forgetPassword}
					>
						Forget Password?
					</Text>
				</View>

				<View style={_styles.footer}>
					<Text>Don't have an account?</Text>
					<Text
						style={_styles.register}
						onPress={register}
					>
						Sign Up
					</Text>
				</View>
			</View>
		</Screen>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		flex: 1,
	},
	container: {
		width: '100%',
		height: '100%'
	},
	header: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',
		marginBottom: '22rem'
	},
	title: {
		fontFamily: Font.bold,
		fontSize: '24rem'
	},
	form: {
		flex: 2.5,
		paddingHorizontal: '22rem',
		marginBottom: '22rem'
	},
	inputPassword: {
		marginTop: '16rem'
	},
	button: {
		marginTop: '32rem'
	},
	forgetPassword: {
		marginTop: '22rem',
		color: '#666'
	},
	footer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'flex-end',
		paddingBottom: '16rem'
	},
	register: {
		color: Color.primary,
		marginLeft: '5rem'
	}
});

export default Login;