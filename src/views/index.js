export { default as Login } from './Auth/Login';
export { default as Register } from './Auth/Register';

export { default as ChangePassword } from './User/ChangePassword';
export { default as EditProfile } from './User/EditProfile';
export { default as Profile } from './User/Profile';

export { default as Home } from './Home/Home';
export { default as Product } from './Home/Product';
export { default as Wishlist } from './Wishlist/Wishlist';
export { default as Transaction } from './Transaction/Transaction';
