import React, { useEffect, useState, useRef } from 'react';
import { View, TextInput, Alert } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useDispatch } from 'react-redux';
import { get, post } from '../../utils/Request';
import { storeUser } from '../../store/user';
import { showLoader, hideLoader } from '../../store/loader';
import { Screen, Button } from '../../components';
import { Color, Font, Size } from '../../assets/styles';

function EditProfile ({ navigation }) {
	const [name, setName] = useState();
	const [email, setEmail] = useState();
	const [phone, setPhone] = useState();
	const [disabled, setDisabled] = useState(true);
    const emailRef = useRef(null);
    const phoneRef = useRef(null);
    const dispatch = useDispatch();

    useEffect(() => {
    	getProfile();
    }, [])

    useEffect(() => {
    	let _disabled = true;
    	if (name && email && phone) {
    		_disabled = false;
    	}
    	setDisabled(_disabled);
    }, [name, email, phone])

    const focusEmail = () => {
        emailRef.current.focus();
    }

    const focusPhone = () => {
        phoneRef.current.focus();
    }

	const getProfile = async () => {
		dispatch(showLoader());

		const response = await get('/user');
		if (response.status === 'success') {
			let user = response.data;
			setName(user.name);
			setEmail(user.email);
			setPhone(user.phone);

			dispatch(hideLoader());
			dispatch(storeUser(user));
		}
		else {
			dispatch(hideLoader());

			let messages = response.messages.join("\n");
			Alert.alert('Oops', messages);
		}
	}

	const updateProfile = async () => {
		if (!disabled) {
			dispatch(showLoader());

			let params = {
				name  : name,
				email : email,
				phone : phone,
			}
			const response = await post('/user/update', params);
			if (response.status === 'success') {
				let user = response.data;

				dispatch(hideLoader());
				dispatch(storeUser(user));

				let messages = response.messages.join("\n");
				Alert.alert('Success', messages);
			}
			else {
				dispatch(hideLoader());

				let messages = response.messages.join("\n");
				Alert.alert('Oops', messages);
			}
		}
	}

	return (
		<Screen contentContainerStyle={_styles.screen}>
			<TextInput
				style={_styles.input}
				placeholder="Name"
				keyboardType="default"
				returnKeyType="next"
				value={name}
				onChangeText={(value) => setName(value)}
				blurOnSubmit={false}
				onSubmitEditing={focusEmail}
			/>
			<TextInput
				ref={emailRef}
				style={_styles.input}
				placeholder="Email"
				keyboardType="email-address"
				autoCompleteType="email"
				autoCapitalize="none"
				returnKeyType="next"
				value={email}
				onChangeText={(value) => setEmail(value)}
				blurOnSubmit={false}
				onSubmitEditing={focusPhone}
			/>
			<TextInput
				ref={phoneRef}
				style={_styles.input}
				placeholder="Phone"
				keyboardType="numeric"
				returnKeyType="send"
				value={phone}
				onChangeText={(value) => setPhone(value)}
				blurOnSubmit={false}
				onSubmitEditing={updateProfile}
			/>

			<View style={_styles.buttonWrapper}>
				<Button
					text="Submit"
					onPress={updateProfile}
					disabled={disabled}
				/>
			</View>
		</Screen>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		flexGrow: 1,
		flexDirection: 'column',
		paddingVertical: '8rem',
		paddingHorizontal: '14rem',
	},
	input: {
		height: Size.inputHeight + 'rem',
		fontFamily: Font.regular,
		fontSize: Size.fontNormal + 'rem',
		borderBottomColor: Color.grey,
		borderBottomWidth: 1,
		paddingHorizontal: 0,
		paddingBottom: '6rem',
		marginBottom: '14rem'
	},
	buttonWrapper: {
		flex: 1,
		justifyContent: 'flex-end',
		marginTop: '32rem',
		paddingBottom: '14rem'
	},
});

export default EditProfile;