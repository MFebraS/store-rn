import React from 'react';
import { View, Text, Alert, Pressable, ScrollView } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useDispatch } from 'react-redux';
import { Screen, Button } from '../../components';
import { get, localLogout } from '../../utils/Request';
import { showLoader, hideLoader } from '../../store/loader';
import { Color, Font } from '../../assets/styles';

function Profile ({ navigation }) {
    const dispatch = useDispatch();

    const editProfile = () => {
    	navigation.navigate('EditProfile');
    }

    const changePassword = () => {
    	navigation.navigate('ChangePassword');
    }

    const address = () => {

    }

	const logout = async () => {
		dispatch(showLoader());

		const response = await get('/auth/logout');
		if (response.status === 'success') {
			dispatch(hideLoader());

			localLogout();
		}
		else {
			dispatch(hideLoader());

			let messages = response.messages.join("\n");
			Alert.alert('Oops', messages);
		}
	}

	return (
		<Screen contentContainerStyle={_styles.screen}>

			<View style={_styles.borderBottom}>
				<Pressable
					style={_styles.listItem}
					android_ripple={_styles.ripple}
					onPress={editProfile}
				>
					<Text>Edit Profile</Text>
				</Pressable>
			</View>
			<View style={_styles.borderBottom}>
				<Pressable
					style={_styles.listItem}
					android_ripple={_styles.ripple}
					onPress={changePassword}
				>
					<Text>Change Password</Text>
				</Pressable>
			</View>
			<View>
				<Pressable
					style={_styles.listItem}
					android_ripple={_styles.ripple}
					onPress={address}
				>
					<Text>Address</Text>
				</Pressable>
			</View>

			<View style={_styles.buttonWrapper}>
				<Button
					text="Logout"
					onPress={() => logout()}
				/>
			</View>

		</Screen>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		flexGrow: 1,
		flexDirection: 'column',
		paddingVertical: '8rem',
	},
	listItem: {
		paddingVertical: '14rem',
		paddingHorizontal: '14rem',
		justifyContent: 'center',
	},
	borderBottom: {
		borderBottomColor: Color.grey,
		borderBottomWidth: 1,
	},
	ripple: {
		color: Color.ripple,
	},
	buttonWrapper: {
		marginTop: '22rem',
		paddingHorizontal: '14rem',
	},
});

export default Profile;