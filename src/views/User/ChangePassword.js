import React, { useEffect, useState, useRef } from 'react';
import { View, Alert } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useDispatch } from 'react-redux';
import { post } from '../../utils/Request';
import { showLoader, hideLoader } from '../../store/loader';
import { Screen, Button, InputIcon } from '../../components';

function ChangePassword ({ navigation }) {
	const [password, setPassword] = useState();
	const [newPassword, setNewPassword] = useState();
	const [newPasswordConfirmation, setNewPasswordConfirmation] = useState();
	const [disabled, setDisabled] = useState(true);
    const newPasswordRef = useRef(null);
    const newPasswordConfirmationRef = useRef(null);
    const dispatch = useDispatch();

    useEffect(() => {
    	let _disabled = true;
    	if (password && newPassword && newPasswordConfirmation &&
    		password.length === 6 && newPassword.length === 6 && newPassword === newPasswordConfirmation) {
    		_disabled = false;
    	}
    	setDisabled(_disabled);
    }, [password, newPassword, newPasswordConfirmation])

    const focusNewPassword = () => {
        newPasswordRef.current.focus();
    }

    const focusNewPasswordConfirmation = () => {
        newPasswordConfirmationRef.current.focus();
    }

	const changePassword = async () => {
		if (!disabled) {
			dispatch(showLoader());

			let params = {
				password  : password,
				new_password : newPassword,
				new_password_confirmation : newPasswordConfirmation,
			}
			const response = await post('/user/change-password', params);
			if (response) {
				dispatch(hideLoader());

				let alert_title = 'Oops';
				if (response.status === 'success') {
					alert_title = 'Success';

					setPassword('');
					setNewPassword('');
					setNewPasswordConfirmation('');
				}

				let messages = response.messages.join("\n");
				Alert.alert(alert_title, messages);
			}
		}
	}

	return (
		<Screen contentContainerStyle={_styles.screen}>
			<InputIcon
				style={_styles.inputIcon}
				styleInput={_styles.input}
				type="password"
				placeholder="Password"
				maxLength={6}
				keyboardType="default"
				autoCompleteType="off"
				autoCapitalize="none"
				returnKeyType="next"
				value={password}
				onChangeText={(value) => setPassword(value)}
				blurOnSubmit={false}
				onSubmitEditing={focusNewPassword}
			/>
			<InputIcon
				ref={newPasswordRef}
				style={_styles.inputIcon}
				styleInput={_styles.input}
				type="password"
				placeholder="New Password"
				maxLength={6}
				keyboardType="default"
				autoCompleteType="off"
				autoCapitalize="none"
				returnKeyType="next"
				value={newPassword}
				onChangeText={(value) => setNewPassword(value)}
				blurOnSubmit={false}
				onSubmitEditing={focusNewPasswordConfirmation}
			/>
			<InputIcon
				ref={newPasswordConfirmationRef}
				style={_styles.inputIcon}
				styleInput={_styles.input}
				type="password"
				placeholder="Re-type New Password"
				maxLength={6}
				keyboardType="default"
				autoCompleteType="off"
				autoCapitalize="none"
				returnKeyType="send"
				value={newPasswordConfirmation}
				onChangeText={(value) => setNewPasswordConfirmation(value)}
				blurOnSubmit={false}
				onSubmitEditing={changePassword}
			/>

			<View style={_styles.buttonWrapper}>
				<Button
					text="Submit"
					onPress={changePassword}
					disabled={disabled}
				/>
			</View>
		</Screen>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		flexGrow: 1,
		flexDirection: 'column',
		paddingVertical: '8rem',
		paddingHorizontal: '14rem',
	},
	inputIcon: {
		paddingHorizontal: 0,
		marginBottom: '14rem'
	},
	input: {
		paddingHorizontal: 0
	},
	buttonWrapper: {
		flex: 1,
		justifyContent: 'flex-end',
		marginTop: '32rem',
		paddingBottom: '14rem'
	},
});

export default ChangePassword;