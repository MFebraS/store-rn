import React from 'react';
import { View, Text, Alert } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Screen, Button } from '../../components';
import { get, localLogout } from '../../utils/Request';

function Transaction ({ navigation }) {
	return (
		<Screen type="view" style={_styles.screen}>

			<View style={_styles.cardWrapper}>
				<Text>Transaction</Text>

			</View>
		</Screen>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		paddingVertical: '16rem',
		paddingHorizontal: '24rem',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	cardWrapper: {
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center'
	}
});

export default Transaction;