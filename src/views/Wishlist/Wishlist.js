import React, { useEffect, useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { Text, FlatList, RefreshControl } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Screen, ProductCard } from '../../components';
import { get } from '../../utils/Request';
import { Color } from '../../assets/styles';

function Wishlist ({ navigation }) {
	const [wishlist, setWishlist] = useState([]);
	const [loader, setLoader] = useState(false);
	const [afterLoad, setAfterLoad] = useState(false);
    const user = useSelector(state => state.user);

    useEffect(() => {
    	const unsubscribe = navigation.addListener('focus', () => {
	    	if (!user) {
				navigation.navigate('Profile');
			}
			else {
				getWishlist();
			}
	    });

	    return unsubscribe;
    }, [navigation, user])

	const getWishlist = async () => {
		setLoader(true);
		setAfterLoad(false);

		const response = await get('/wishlist');
		if (response) {
			if (response.status === 'success') {
				setWishlist(response.data);
			}
			setLoader(false);
			setAfterLoad(true);
		}
	}

    const onRefresh = useCallback(() => {
    	setWishlist([]);
    	getWishlist();
    }, [loader])

	const detailProduct = (productId) => {
		navigation.navigate('Product', {
            productId: productId,
        });
	}

	return (
		<Screen type="view" style={_styles.screen}>
			<FlatList
				contentContainerStyle={_styles.flatListContent}
				columnWrapperStyle={_styles.flatlistColumn}
				numColumns={2}
				horizontal={false}
				data={wishlist}
				renderItem={({ item }) => (
					<ProductCard
						id={item.id}
						name={item.name}
						image={item.image}
						price={item.price}
						wishlist={true}
						onPress={() => detailProduct(item.id)}
					/>
				)}
				keyExtractor={(item, index) => item.id}
				ListEmptyComponent={() => (afterLoad ? <Text style={_styles.noData}>No Wishlist</Text> : null)}
				refreshControl={
		        	<RefreshControl colors={[Color.primary]} refreshing={loader} onRefresh={onRefresh} />
		        }
			/>
		</Screen>
	);
}

const _styles = EStyleSheet.create({
	screen: {
		flex: 1
	},
	flatListContent: {
		paddingTop: '14rem',
		paddingBottom: '10rem',
		paddingHorizontal: '12rem'
	},
	flatlistColumn: {
		justifyContent: 'space-between',
		marginBottom: '14rem'
	},
	noData: {
		textAlign: 'center',
		fontStyle: 'italic',
		color: Color.grey,
		marginTop: '16rem'
	}
});

export default Wishlist;