import React, { Fragment } from 'react';
import { Image, View, ActivityIndicator, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSelector } from 'react-redux';
import { SearchContextProvider } from '../contexts';
import { Color, Font, Size } from '../assets/styles';
import {
	Login,
	Register,
	Home,
	Product,
	Wishlist,
	Transaction,
	Profile,
	EditProfile,
	ChangePassword,
} from '../views';

const Tab = createBottomTabNavigator();
const AppStack = createStackNavigator();
const WishlistStack = createStackNavigator();
const TransactionStack = createStackNavigator();
const ProfileStack = createStackNavigator();

function HomeTabScreen() {
	return (
		<SearchContextProvider>
			<Tab.Navigator
				screenOptions = {({ route }) => ({
					tabBarIcon: ({ focused, color, size }) => {
						let icon;

						if (route.name === 'Home') {
							icon = focused
							? require('../assets/icons/home-active.png')
							: require('../assets/icons/home.png');
						}
						else if (route.name === 'Wishlist') {
							icon = focused
							? require('../assets/icons/heart-active.png')
							: require('../assets/icons/heart.png');
						}
						else if (route.name === 'Transaction') {
							icon = focused
							? require('../assets/icons/inbox-active.png')
							: require('../assets/icons/inbox.png');
						}
						else if (route.name === 'Profile') {
							icon = focused
							? require('../assets/icons/user-active.png')
							: require('../assets/icons/user.png');
						}

						return <Image style={{width: 20, height: 20}} source={icon} resizeMode="center" />;
					},
				})}
				tabBarOptions={{
					keyboardHidesTabBar: true,
					activeTintColor: Color.primary,
					inactiveTintColor: 'gray',
					style:{
						borderTopLeftRadius: 16,
						borderTopRightRadius: 16
					},
					labelStyle: {
						fontSize: 11
					},
				}}
			>
				<Tab.Screen name="Home" component={Home} />
				<Tab.Screen name="Wishlist" component={WishlistStackScreen} />
				<Tab.Screen name="Transaction" component={TransactionStackScreen} />
				<Tab.Screen name="Profile" component={ProfileStackScreen} />
			</Tab.Navigator>
		</SearchContextProvider>
	);
}

function WishlistStackScreen() {
	return (
		<WishlistStack.Navigator>
			<WishlistStack.Screen name="Wishlist" component={Wishlist} />
		</WishlistStack.Navigator>
	);
}

function TransactionStackScreen() {
	return (
		<TransactionStack.Navigator>
			<TransactionStack.Screen name="Transaction" component={Transaction} />
		</TransactionStack.Navigator>
	);
}

function ProfileStackScreen() {
    const user = useSelector(state => state.user);

	return (
		<ProfileStack.Navigator>
			{ user ? (
				<Fragment>
					<ProfileStack.Screen name="Profile" component={Profile} />
					<ProfileStack.Screen name="EditProfile" component={EditProfile} options={{ title: 'Edit Profile' }} />
					<ProfileStack.Screen name="ChangePassword" component={ChangePassword} options={{ title: 'Change Password' }} />
				</Fragment>
			) : (
				<Fragment>
					<ProfileStack.Screen name="Login" component={Login} options={{ headerShown: false }} />
					<ProfileStack.Screen name="Register" component={Register} options={{ headerShown: false }} />
				</Fragment>
			) }
		</ProfileStack.Navigator>
	);
}

function Router() {
    const loader = useSelector(state => state.loader);

	return (
		<NavigationContainer>
			
			<AppStack.Navigator>
				<AppStack.Screen name="Home" component={HomeTabScreen} options={{ headerShown: false }} />
				<AppStack.Screen name="Product" component={Product} options={{ headerShown: false }} />
			</AppStack.Navigator>

			{loader > 0 &&
				<View style={_styles.loader}>
					<ActivityIndicator size="large" color={Color.primary} />
				</View>
			}
		</NavigationContainer>
	);
}

const _styles = StyleSheet.create({
	loader: {
		position: 'absolute',
		width: '100%',
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(255, 255, 255, 0.5)'
	}
});

export default Router;
