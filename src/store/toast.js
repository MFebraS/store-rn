const STORE_TOAST = 'STORE_TOAST';
const CLEAR_TOAST = 'CLEAR_TOAST';

export function storeToast(data) {
	return {
		type: STORE_TOAST,
		data
	}
}

export function clearToast() {
	return {
		type: CLEAR_TOAST
	}
}

const defaultToast = {};

function toast(state=defaultToast, action) {
	switch (action.type) {
		case STORE_TOAST:
			return action.data;
			break;
		case CLEAR_TOAST:
			return {};
			break;
		default:
			return state;
			break;
	}
}

export default toast;