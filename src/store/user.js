import { setData, getData, clearData } from '../utils/Storage';

const STORE_USER = 'STORE_USER';
const CLEAR_USER = 'CLEAR_USER';

export function storeUser(data) {
	return {
		type: STORE_USER,
		data
	}
}

export function clearUser() {
	return {
		type: CLEAR_USER
	}
}

const default_state = getData('user');

function user(state=default_state, action) {
	switch (action.type) {
		case STORE_USER:
			setData('user', action.data);
			return action.data;
			break;
		case CLEAR_USER:
			clearData('user');
			return null;
			break;

		default:
			return state;
			break;
	}
}

export default user;
