import { createStore, combineReducers } from 'redux';
import loader from './loader';
import toast from './toast';
import token from './token';
import user from './user';

const reducers = combineReducers({
    loader: loader,
    toast: toast,
    token: token,
    user: user,
});

const store = createStore(reducers);

export default store;