import axios from 'axios';
import { setData, getData, clearData } from '../utils/Storage';

const STORE_TOKEN = 'STORE_TOKEN';
const CLEAR_TOKEN = 'CLEAR_TOKEN';

export function storeToken(data) {
	return {
		type: STORE_TOKEN,
		data
	}
}

export function clearToken() {
	return {
		type: CLEAR_TOKEN
	}
}

const default_state = getData('token');

function token(state=default_state, action) {
	switch (action.type) {
		case STORE_TOKEN:
			if (action.data) {
				setData('token', action.data);
				axios.defaults.headers.common['Authorization'] = `Bearer ${action.data}`;
			}
			return action.data;
			break;
		case CLEAR_TOKEN:
			clearData('token');
			delete axios.defaults.headers.common['Authorization'];
			return null;
			break;

		default:
			return state;
			break;
	}
}

export default token;