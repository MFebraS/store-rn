import React, { useContext } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, Platform } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Search } from '../';
import { SearchContext } from '../../contexts';
import { Color, Font, Size } from '../../assets/styles';

const { height: screenHeight } = Dimensions.get('window');
const isIphoneX = screenHeight === 812 || screenHeight === 896;
const navbarHeight = Platform.OS === 'ios' ? (isIphoneX ? 88 : 64) : 64;

function HomeNavbar ({ brand }) {
	const bellIcon = require('../../assets/icons/bell.png');
	const chatIcon = require('../../assets/icons/chat.png');
	const cartIcon = require('../../assets/icons/cart.png');
	const searchContext = useContext(SearchContext);

	const onClear = () => {
		searchContext.setKeyword('');
		searchContext.setSearch(true);
	}

	return (
		<View style={_styles.navbar}>
			<View style={_styles.navbarLeft}>
	    		{ brand ?
		    		<Text style={_styles.brand}>Store</Text>
	    		:
		    		<Search
		    			value={searchContext.keyword}
		    			returnKeyType="search"
		    			onChangeText={(e) => searchContext.setKeyword(e)}
		    			onSearch={() => searchContext.setSearch(true)}
			    		onSubmitEditing={() => searchContext.setSearch(true)}
	    				onClear={onClear}
		    		/>
		    	}
			</View>
			<View style={_styles.navbarRight}>
				<TouchableOpacity
					style={_styles.buttonIcon}
					activeOpacity={0.5}
					onPress={() => {}}
				>
					<Image style={_styles.icon} source={bellIcon} resizeMode="center" />
				</TouchableOpacity>
				<TouchableOpacity
					style={_styles.buttonIcon}
					activeOpacity={0.5}
					onPress={() => {}}
				>
					<Image style={_styles.icon} source={chatIcon} resizeMode="center" />
				</TouchableOpacity>
				<TouchableOpacity
					style={_styles.buttonIcon}
					activeOpacity={0.5}
					onPress={() => {}}
				>
					<Image style={_styles.icon} source={cartIcon} resizeMode="center" />
				</TouchableOpacity>
			</View>
		</View>
	);
}

const _styles = EStyleSheet.create({
	navbar: {
		height: navbarHeight,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingHorizontal: '10rem',
		backgroundColor: Color.primary
	},
	brand: {
		fontFamily: Font.brand,
		fontSize: '22rem',
		color: '#fff'
	},
	navbarLeft: {
		flexGrow: 1,
		marginRight: '10rem'
	},
	navbarRight: {
		flexDirection: 'row'
	},
	buttonIcon: {
		width: (Size.buttonIconHeight - 2) + 'rem',
		height: Size.buttonIconHeight + 'rem',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#fff',
		borderRadius: '8rem',
		marginHorizontal: '3rem'
	},
	ripple: {
		color: Color.ripple,
	},
	icon: {
		width: '18rem',
		height: '18rem'
	}
});

export default HomeNavbar;