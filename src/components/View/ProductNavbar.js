import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { View, Image, TouchableOpacity, Dimensions, Platform } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { post } from '../../utils/Request';
import { Color, Size } from '../../assets/styles';

const { height: screenHeight } = Dimensions.get('window');
const isIphoneX = screenHeight === 812 || screenHeight === 896;
const navbarHeight = Platform.OS === 'ios' ? (isIphoneX ? 88 : 64) : 64;

function ProductNavbar ({ transparent, productId, wishlist:favorite, onBack=null }) {
	const arrowLeftIcon = require('../../assets/icons/arrow-left.png');
	const arrowLeftWhiteIcon = require('../../assets/icons/arrow-left-white.png');
	const heartWhiteIcon = require('../../assets/icons/heart-white.png');
	const heartRedIcon = require('../../assets/icons/heart-red.png');
	const heartIcon = require('../../assets/icons/heart.png');
	const cartWhiteIcon = require('../../assets/icons/cart-white.png');
	const cartIcon = require('../../assets/icons/cart.png');
	const [wishlist, setWishlist] = useState(favorite);
	const [disableWishlist, setDisableWishlist] = useState(false);
    const user = useSelector(state => state.user);
    const navigation = useNavigation();

	const addWishlist = async () => {
		if (!user) {
			return navigation.navigate('Profile');
		}

		setDisableWishlist(true);

		let url = '/wishlist/add';
		let params = {
			product_id: productId
		}
		const response = await post(url, params);
		if (response) {
			if (response.status === 'success') {
				setWishlist(true);
			}
			setDisableWishlist(false);
		}
	}

	const removeWishlist = async () => {
		setDisableWishlist(true);

		let url = '/wishlist/delete';
		let params = {
			product_id: productId
		}
		const response = await post(url, params);
		if (response) {
			if (response.status === 'success') {
				setWishlist(false);
			}
			setDisableWishlist(false);
		}
	}

	return (
		<View style={[_styles.navbar, (transparent ? _styles.transparent : '')]}>
			<View style={_styles.navbarLeft}>
	    		<TouchableOpacity
					style={_styles.buttonIcon}
					activeOpacity={0.5}
					onPress={onBack}
				>
					<Image style={_styles.icon} source={transparent ? arrowLeftWhiteIcon : arrowLeftIcon} resizeMode="center" />
				</TouchableOpacity>
			</View>
			<View style={_styles.navbarRight}>
				<TouchableOpacity
					style={_styles.buttonIcon}
					activeOpacity={0.5}
					disabled={disableWishlist}
					onPress={wishlist ? removeWishlist : addWishlist }
				>
					<Image style={_styles.icon} source={wishlist ? heartRedIcon : (transparent ? heartWhiteIcon : heartIcon)} resizeMode="center" />
				</TouchableOpacity>
				<TouchableOpacity
					style={_styles.buttonIcon}
					activeOpacity={0.5}
					onPress={() => {}}
				>
					<Image style={_styles.iconCart} source={transparent ? cartWhiteIcon : cartIcon} resizeMode="center" />
				</TouchableOpacity>
			</View>
		</View>
	);
}

const _styles = EStyleSheet.create({
	navbar: {
		height: navbarHeight,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingHorizontal: '10rem',
		backgroundColor: '#fff'
	},
	transparent: {
		backgroundColor: 'transparent'
	},
	navbarLeft: {
		flexGrow: 1,
		marginRight: '10rem'
	},
	navbarRight: {
		flexDirection: 'row'
	},
	buttonIcon: {
		width: Size.buttonIconHeight + 'rem',
		height: Size.buttonIconHeight + 'rem',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'transparent',
		marginHorizontal: '3rem'
	},
	icon: {
		width: '22rem',
		height: '22rem'
	},
	iconCart: {
		width: '25rem',
		height: '25rem'
	}
});

ProductNavbar.propTypes = {
	transparent: PropTypes.bool,
	productId: PropTypes.number,
	onBack: PropTypes.func,
};

export default ProductNavbar;
