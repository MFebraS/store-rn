import React from 'react';
import { SafeAreaView, StatusBar, StyleSheet } from 'react-native';
import { Color } from '../../assets/styles';

function CustomStatusBar ({ style, backgroundColor=Color.darkPrimary, ...props }) {
	return (
		<SafeAreaView style={style}>
			<StatusBar backgroundColor={backgroundColor} {...props} />
		</SafeAreaView>
	);
}

export default CustomStatusBar;