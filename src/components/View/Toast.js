import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { clearToast } from '../../store/toast';
import { Color } from '../../assets/styles';

function Toast ({ style=null, show=false, type="", message="" }) {
    const dispatch = useDispatch();
	const closeWhiteIcon = require('../../assets/icons/close-white.png');
	const textClass = type == 'success' ? 'textSuccess' : 'textError';

    useEffect(() => {
    	if (show == true) {
    		let timer = setTimeout(function(){
				dispatch(clearToast());
    		}, 5000);

    		return () => {
				clearTimeout(timer);
			};
    	}
    }, [show])

    const onClose = () => {
		dispatch(clearToast());
    }

	return (
		<Fragment>
			{show ?
				<View style={[_styles.toast, _styles[type]]}>
					<Text style={_styles[textClass]}>
						{ message }
					</Text>
					<TouchableOpacity
						style={_styles.closeButton}
						activeOpacity={0.5}
						onPress={(onClose)}
					>
						<Image style={_styles.buttonIcon} source={closeWhiteIcon} resizeMode="center" />
					</TouchableOpacity>
				</View>
				:
				null
			}
		</Fragment>
	);
}

const _styles = EStyleSheet.create({
	toast: {
		width: '100%',
		minHeight: '50rem',
		borderBottomWidth: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: '12rem',
		position: 'absolute',
		top: 0,
		zIndex: 100
	},
	success: {
		backgroundColor: Color.lightSuccess,
		borderBottomColor: Color.success
	},
	error: {
		backgroundColor: Color.lightDanger,
		borderBottomColor: Color.danger
	},
	textSuccess: {
		color: Color.success
	},
	textError: {
		color: Color.danger
	},
	closeButton: {
		width: '20rem',
		height: '20rem',
		borderWidth: 1,
		borderColor: '#fff',
		borderRadius: '10rem',
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: '5rem'
	},
	buttonIcon: {
		width: '15rem',
		height: '15rem'
	}
});

Toast.propTypes = {
	style: PropTypes.oneOfType([
		PropTypes.object,
		PropTypes.array
	]),
	type: PropTypes.string,
	show: PropTypes.bool,
	message: PropTypes.string,
};

export default Toast;