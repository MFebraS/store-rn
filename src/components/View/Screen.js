import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, View, ScrollView, StyleSheet } from 'react-native';

function Screen ({ style=null, contentContainerStyle=null, type="", children }) {
	return (
		<SafeAreaView style={_styles.screen}>
			{type === "view" ?
				<View style={style} >
					{ children }
				</View>
				:
				<ScrollView
					style={style}
					contentContainerStyle={contentContainerStyle}
				>
					{ children }
				</ScrollView>
			}
		</SafeAreaView>
	);
}

const _styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: '#fff'
	}
});

Screen.propTypes = {
	style: PropTypes.oneOfType([
		PropTypes.object,
		PropTypes.array
	]),
	type: PropTypes.string,
};

export default Screen;