import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Pressable } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Color, Size } from '../../assets/styles';

function ProductCarousel ({ sliderWidth, itemWidth, images=[], onShare=null }) {
	const [activeSlide, setActiveSlide] = useState(0);
    const carouselRef = useRef(null);
	const shareIcon = require('../../assets/icons/share.png');

	const carouselItem = ({item, index}) => {
        return (
			<Image style={_styles.image} source={{uri: item}} resizeMode="cover" />
        );
    }

	return (
		<View style={_styles.container}>
			<Carousel
            	ref={carouselRef}
            	data={images}
            	renderItem={carouselItem}
            	sliderWidth={sliderWidth}
            	itemWidth={itemWidth}
            	onSnapToItem={(index) => setActiveSlide(index) }
            />

			<Pagination
				dotsLength={images.length}
				activeDotIndex={activeSlide}
				containerStyle={_styles.pagination}
				dotStyle={_styles.paginationDot}
				inactiveDotOpacity={0.5}
				inactiveDotScale={0.6}
			/>

			<Pressable
				style={_styles.fab}
				android_ripple={_styles.ripple}
				onPress={onShare}
			>
				<Image style={_styles.fabIcon} source={shareIcon} resizeMode="contain" />
			</Pressable>
		</View>
	);
}

const _styles = EStyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	image: {
		width: '100%',
		height: '100%'
	},
	ripple: {
		color: Color.ripple,
	},
	fab: {
		width: Size.buttonIconHeight + 'rem',
		height: Size.buttonIconHeight + 'rem',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#fff',
		borderRadius: Size.buttonIconHeight/2 + 'rem',
		position: 'absolute',
		bottom: '16rem',
		right: '16rem',
		elevation: 5,
		overflow: 'hidden'
	},
	fabIcon: {
		width: '20rem',
		height: '20rem',	
	},
	pagination: {
		position: 'absolute', 
		bottom: 0,
		backgroundColor: 'transparent'
	},
	paginationDot: {
		width: '8rem',
		height: '8rem',
		borderRadius: '4rem',
		marginHorizontal: 0,
		backgroundColor: '#fff'
	}
});

ProductCarousel.propTypes = {
	sliderWidth: PropTypes.number,
	itemWidth: PropTypes.number,
	images: PropTypes.array,
	onShare: PropTypes.func
};

export default ProductCarousel;
