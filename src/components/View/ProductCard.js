import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { View, Text, Image, TouchableOpacity, Pressable } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { numberFormat } from '../../utils/Helpers';
import { post } from '../../utils/Request';
import { Color, Font } from '../../assets/styles';

function ProductCard ({ id, name, price, image:img, wishlist:favorite, onPress=null }) {
	const heartIcon = require('../../assets/icons/heart.png');
	const heartRedIcon = require('../../assets/icons/heart-red.png');
	const image = img ? {uri: img} : null;
	const [wishlist, setWishlist] = useState(favorite);
	const [disableWishlist, setDisableWishlist] = useState(false);
    const user = useSelector(state => state.user);
    const navigation = useNavigation();

	const addWishlist = async () => {
		if (!user) {
			return navigation.navigate('Profile');
		}

		setDisableWishlist(true);

		let url = '/wishlist/add';
		let params = {
			product_id: id
		}
		const response = await post(url, params);
		if (response) {
			if (response.status === 'success') {
				setWishlist(true);
			}
			setDisableWishlist(false);
		}
	}

	const removeWishlist = async () => {
		setDisableWishlist(true);

		let url = '/wishlist/delete';
		let params = {
			product_id: id
		}
		const response = await post(url, params);
		if (response) {
			if (response.status === 'success') {
				setWishlist(false);
			}
			setDisableWishlist(false);
		}
	}

	const addCart = () => {
		
	}

	return (
		<View style={_styles.card}>
			<TouchableOpacity
				style={_styles.wrapper}
				activeOpacity={0.5}
				onPress={onPress}
			>
				<Image style={_styles.image} source={image} resizeMode="cover" />

				<View style={_styles.info}>
					<Text style={_styles.name}>{name}</Text>
					<Text style={_styles.price}>{numberFormat(price, 'Rp')}</Text>
				</View>
			</TouchableOpacity>

			<TouchableOpacity
				style={_styles.wishlist}
				activeOpacity={0.5}
				disabled={disableWishlist}
				onPress={wishlist ? removeWishlist : addWishlist}
			>
				<Image style={_styles.wishlistIcon} source={wishlist ? heartRedIcon : heartIcon} resizeMode="contain" />
			</TouchableOpacity>

			<Pressable
				style={_styles.addCart}
				android_ripple={_styles.ripple}
				onPress={addCart}
			>
				<Text style={_styles.addCartText}>+</Text>
			</Pressable>
		</View>
	);
}

const _styles = EStyleSheet.create({
	card: {
		width: '48%',
		flexDirection: 'column',
		backgroundColor: '#fff',
		borderRadius: '8rem',
		shadowColor: '#000',
		shadowOffset: { width: 0, height: '3rem' },
		shadowOpacity: 0.5,
		shadowRadius: '5rem',
		elevation: 3,
		overflow: 'hidden'
	},
	wrapper: {
		width: '100%'
	},
	image: {
		width: '100%',
		height: '140rem'
	},
	info: {
		paddingVertical: '4rem',
		paddingHorizontal: '8rem',
		marginTop: '4rem'
	},
	name: {
		height: '35rem',
		fontSize: '14rem',
		lineHeight: '20rem',
		marginBottom: '6rem',
		color: Color.darkGrey,
	},
	price: {
		width: '75%',
		height: '32rem',
		textAlignVertical: 'center'
	},
	ripple: {
		color: Color.ripple,
	},
	wishlist: {
		width: '32rem',
		height: '32rem',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#fff',
		borderRadius: '16rem',
		position: 'absolute',
		top: '6rem',
		right: '6rem',
		elevation: 5,
	},
	wishlistIcon: {
		width: '17rem',
		height: '17rem',	
	},
	addCart: {
		width: '30rem',
		height: '30rem',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: Color.primary,
		borderRadius: '8rem',
		position: 'absolute',
		right: '6rem',
		bottom: '6rem',
	},
	addCartText: {
		fontFamily: Font.bold,
		fontSize: '24rem',
		color: '#fff'
	}
});

ProductCard.propTypes = {
	id: PropTypes.number,
	name: PropTypes.string,
	price: PropTypes.string,
	image: PropTypes.string,
	onPress: PropTypes.func,
};

export default ProductCard;