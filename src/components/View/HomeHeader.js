import React, { useContext } from 'react';
import { ScrollView, View, Text, TouchableOpacity, Dimensions, Platform } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Search } from '../';
import { SearchContext } from '../../contexts';
import { Color } from '../../assets/styles';

const { height: screenHeight } = Dimensions.get('window');
const isIphoneX = screenHeight === 812 || screenHeight === 896;
const navbarHeight = Platform.OS === 'ios' ? (isIphoneX ? 88 : 64) : 64;

function HomeHeader ({ data=[], activeLabel, onLabelPress=null }) {
	const searchContext = useContext(SearchContext);

	const onClear = () => {
		searchContext.setKeyword('');
		searchContext.setSearch(true);
	}

	return (
	    <View style={_styles.header}>
	    	<Search
    			value={searchContext.keyword}
	    		returnKeyType="search"
    			onChangeText={(e) => searchContext.setKeyword(e)}
    			onSearch={() => searchContext.setSearch(true)}
	    		onSubmitEditing={() => searchContext.setSearch(true)}
	    		onClear={onClear}
	    	/>
	    	<ScrollView
	    		contentContainerStyle={_styles.categoryWrapper}
	    		horizontal={true}
	    		showsHorizontalScrollIndicator={false}
	    	>
	    		<TouchableOpacity
	    			activeOpacity={0.5}
	    			onPress={() => onLabelPress('')}
	    		>
		    		<Text style={[ _styles.label, (activeLabel == '' ? _styles.labelActive : null) ]}>All</Text>
	    		</TouchableOpacity>

	    		{data.map((item, index) => (
		    		<TouchableOpacity
		    			key={index}
		    			activeOpacity={0.5}
		    			onPress={() => onLabelPress(item.id)}
		    		>
			    		<Text style={[ _styles.label, (activeLabel == item.id ? _styles.labelActive : null) ]}>{item.name}</Text>
		    		</TouchableOpacity>
	    		))}
	    	</ScrollView>
	    </View>
	);
}

const _styles = EStyleSheet.create({
	header: {
		width: '100%',
		paddingHorizontal: '10rem',
		marginTop: navbarHeight,
		paddingTop: '10rem'
	},
	categoryWrapper: {
		flexDirection: 'row',
		alignItems: 'center',
		marginTop: '22rem'
	},
	label: {
		minWidth: '50rem',
		borderRadius: '16rem',
		color: Color.regular,
		backgroundColor: '#fff',
		paddingHorizontal: '11rem',
		paddingTop: '5rem',
		paddingBottom: '3rem',
		marginRight: '8rem',
		textAlign: 'center'
	},
	labelActive: {
		color: Color.primary
	}
});

export default HomeHeader;