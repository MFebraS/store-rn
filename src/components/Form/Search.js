import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { View, Image, TextInput, TouchableOpacity } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Font, Size } from '../../assets/styles';

const Search = forwardRef(({ style=null, value='', onSearch=null, onClear=null, ...props }, ref) => {
	const searchIcon = require('../../assets/icons/search.png');
	const closeIcon = require('../../assets/icons/close.png');

	return (
		<View style={[_styles.container, style]}>
			<TouchableOpacity
				style={_styles.iconWrapper}
				activeOpacity={0.5}
				onPress={onSearch}
			>
				<Image
					style={_styles.icon}
					source={searchIcon}
					resizeMode="center"
				/>
			</TouchableOpacity>

			<TextInput
				ref={ref}
				style={_styles.input}
				placeholder="Search..."
				value={value}
				{...props}
			/>

			{ value !== '' &&
				<TouchableOpacity
					style={_styles.iconWrapper}
					activeOpacity={0.5}
					onPress={onClear}
				>
					<Image
						style={_styles.icon}
						source={closeIcon}
						resizeMode="center"
					/>
				</TouchableOpacity>
			}
		</View>
	);
})

const _styles = EStyleSheet.create({
	container: {
		paddingHorizontal: '8rem',
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: '#fff',
		borderRadius: '10rem',
	},
	iconWrapper: {
		height: '100%',
		paddingHorizontal: '4rem',
		flexDirection: 'row',
		alignItems: 'center',
	},
	icon: {
		width: '15rem',
		height: '15rem'
	},
	input: {
		flex: 1,
		height: Size.inputHeight + 'rem',
		marginHorizontal: '6rem',
		fontFamily: Font.regular,
		fontSize: Size.fontNormal + 'rem',
		paddingBottom: '6rem',
	}
});

Search.propTypes = {
	style: PropTypes.oneOfType([
		PropTypes.object,
		PropTypes.array
	]),
	onChangeText: PropTypes.func,
	onSearch: PropTypes.func,
};

export default Search;