import React from 'react';
import PropTypes from 'prop-types';
import { Text, Pressable } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Color, Font, Size } from '../../assets/styles';

function Button ({ style=null, text="", color="primary", disabled=false, onPress=null, ...props }) {
	return (
		<Pressable
			style={[_styles.button, style, disabled ? _styles.disabled : {}, {backgroundColor: Color[color]} ]}
			android_ripple={_styles.ripple}
			disabled={disabled}
			onPress={onPress}
			{...props}
		>
			<Text style={_styles.text}>{text}</Text>
		</Pressable>
	);
}

const _styles = EStyleSheet.create({
	button: {
		height: Size.buttonHeight + 'rem',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: Size.buttonHeight / 2 + 'rem',
		padding: '8rem'
	},
	ripple: {
		color: Color.ripple,
	},
	disabled: {
		opacity: 0.7,
	},
	text: {
		color: '#fff',
		fontFamily: Font.bold,
		fontSize: Size.buttonFontSize + 'rem'
	}
});

Button.propTypes = {
	style: PropTypes.oneOfType([
		PropTypes.object,
		PropTypes.array
	]),
	text: PropTypes.string,
	onPress: PropTypes.func,
};

export default Button;