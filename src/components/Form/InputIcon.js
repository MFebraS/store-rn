import React, { useEffect, useState, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { View, Image, TextInput, TouchableOpacity } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Color, Font, Size } from '../../assets/styles';

const InputIcon = forwardRef(({ style=null, styleInput=null, type="", icon=null, onChangeText=null, ...props }, ref) => {
	const [viewPassword, setViewPassword] = useState(false);
	const eyeIcon = require('../../assets/icons/eye.png');
	const eyeOffIcon = require('../../assets/icons/eye-off.png');

	return (
		<View style={[_styles.container, style]}>
			{icon &&
				<Image
					style={[_styles.icon, _styles.marginRight]}
					source={icon}
					resizeMode="center"
				/>
			}

			<TextInput
				ref={ref}
				style={[_styles.input, styleInput]}
				onChangeText={onChangeText}
				secureTextEntry={type === 'password' && !viewPassword ? true : false}
				{...props}
			/>

			{type === 'password' &&
				<TouchableOpacity
					style={_styles.viewPassword}
					onPress={() => setViewPassword(!viewPassword)}
				>
					<Image
						style={_styles.icon}
						source={ viewPassword ? eyeOffIcon : eyeIcon }
						resizeMode="center"
					/>
				</TouchableOpacity>
			}
		</View>
	);
})

const _styles = EStyleSheet.create({
	container: {
		paddingHorizontal: '8rem',
		flexDirection: 'row',
		alignItems: 'center',
		borderBottomWidth: 1,
		borderBottomColor: Color.grey,
	},
	icon: {
		width: '15rem',
		height: '15rem',
	},
	marginRight: {
		marginRight: '8rem',
	},
	input: {
		flex: 1,
		height: Size.inputHeight + 'rem',
		fontFamily: Font.regular,
		fontSize: Size.fontNormal + 'rem',
		paddingBottom: '6rem',
		marginRight: '8rem',
	},
	viewPassword: {
		height: '100%',
		paddingHorizontal: '4rem',
		flexDirection: 'row',
		alignItems: 'center',
	}
});

InputIcon.propTypes = {
	style: PropTypes.oneOfType([
		PropTypes.object,
		PropTypes.array
	]),
	styleInput: PropTypes.oneOfType([
		PropTypes.object,
		PropTypes.array
	]),
	type: PropTypes.string,
	onChangeText: PropTypes.func,
};

export default InputIcon;