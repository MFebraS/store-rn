import axios from 'axios';
import store from '../store';
import { clearUser } from '../store/user';
import { clearToken } from '../store/token';
import { API_URL, DEBUG } from '../../env.json';

/**
 * GET method for request API
 */
export const get = async (url) => {
	url = API_URL + url;
	if (DEBUG) {
		console.log('======= GET =======');
		console.log('======= URL :');
		console.log(url);
	}

	try {
		const response = await axios({
			method	: 'get',
			url 	: url,
			timeout : 5000,    // 5 seconds timeout
			headers : {
				'Accept': 'application/json',
			}
		});

		// if request success
		if (DEBUG) {
			console.log('======= RESPONSE :')
			if (Array.isArray(response.data.data)) {
				response.data.data.forEach( function(item, index) {
					console.log(item)
				});
			}
			else {
				console.log(response.data)
			}
		}
		return response.data;

	} catch(e) {
		// if request fail
		if (DEBUG) {
			console.log('======= ERROR :');
			console.log(e + "\n");
		}
		if (e.response.status === 401 && e.response.data.message === 'Unauthenticated.') {
			localLogout();
		}

		return {
			status 	 : e.response.status || 'fail',
			messages : e.response.data.messages || [e.message]
		}
	}

}

/**
 * POST method for request API
 */
export const post = async (url, params, type='json') => {
	url = API_URL + url;
	if (DEBUG) {
		console.log('======= POST =======');
		console.log('======= URL :');
		console.log(url);
		console.log('======= PARAMS :');
		console.log(params);
	}

	let headers = {
		'Accept': 'application/json'
	}

	if (type === 'form-data') {
		headers['Content-Type'] = 'multipart/form-data'

		// create form data
		let form_data = new FormData()
		Object.entries(params).forEach(entry => {
			let [key, value] = entry

			if (value) {
				if (Array.isArray(value)) {
					value.forEach( function(element, index) {
						form_data.append(`${key}[]`, element);
					})
				}
				else {
					form_data.append(key, value)
				}

				if (DEBUG) {
					console.log('======= POST PARAMS FORM :', KEY, VALUE)
				}
			}
		})

		params = form_data
	}

	try {
		const response = await axios({
			method	: 'post',
			url 	: url,
			timeout : 5000,    // 5 seconds timeout
			headers : headers,
			data 	: params
		});

		// if request success
		if (DEBUG) {
			console.log('======= RESPONSE :')
			if (Array.isArray(response.data.data)) {
				response.data.data.forEach( function(item, index) {
					console.log(item)
				});
			}
			else {
				console.log(response.data)
			}
		}
		return response.data;

	} catch(e) {
		// if request fail
		if (DEBUG) {
			console.log('======= ERROR :');
			console.log(e + "\n");
		}
		if (e.response.status === 401 && e.response.data.message === 'Unauthenticated.') {
			localLogout();
		}

		return {
			status 	 : e.response.status || 'fail',
			messages : e.response.data.messages || [e.message]
		}
	}

}

/**
 * Remove data from redux & storage
 */
export const localLogout = () => {
	store.dispatch(clearUser());
	store.dispatch(clearToken());
}
