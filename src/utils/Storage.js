import { MMKV } from 'react-native-mmkv';
import { encrypt, decrypt } from 'react-native-simple-encryption';
import { ENCRYPT_KEY } from '../../env.json';

export const setData = (key, value, _encrypt=true) => {
	if (value) {
		// stringify
		value = JSON.stringify(value);

		if (_encrypt) {
			value = encrypt(ENCRYPT_KEY, value);
		}
	}

	MMKV.set(key, value);
}

export const getData = (key, _decrypt=true) => {
	// get data
	let value = MMKV.getString(key);

	if (value) {
		if (_decrypt) {
			value = decrypt(ENCRYPT_KEY, value);
		}

		// parse string
		value = JSON.parse(value);
	}

	return value || null;
}

export const clearData = (key) => {
	MMKV.delete(key);
}
