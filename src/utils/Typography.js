import React from 'react';
import { Text, Platform } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Color, Font, Size } from '../assets/styles';

export const typography = () => {
	const oldTextRender = Text.render;
	Text.render = function(...args) {
		const origin = oldTextRender.call(this, ...args);
		return React.cloneElement(origin, {
			style: [styles.defaultText, origin.props.style],
		});
	}
}

const styles = EStyleSheet.create({
	defaultText: {
		fontFamily: Font.regular,
		fontSize: Size.fontNormal + 'rem',
		color: Color.regular
	}
});