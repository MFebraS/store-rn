export const numberFormat = (val, prefix='') => {
	if (!val) {
		return null;
	}

	if (prefix) {
		prefix += ' ';
	}

    return prefix + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
