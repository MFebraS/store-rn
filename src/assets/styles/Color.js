export const Color = {
	primary: '#a6deee',
	darkPrimary: '#8CD3E6',
	regular: '#222',
	darkGrey: '#484444',
	grey: '#CCC',
	red: '#E5182C',
	success: '#56DD7B',
	lightSuccess: '#D8F9E1',
	danger: '#EE5A5A',
	lightDanger: '#F9D8D8',
	ripple: 'rgba(255, 255, 255, 0.5)'
}