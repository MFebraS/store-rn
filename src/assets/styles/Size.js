export const Size = {
	fontSmall: 11,
	fontNormal: 13,
	fontTitle: 18,
	inputHeight: 38,
	buttonHeight: 42,
	buttonIconHeight: 37,
	buttonFontSize: 15,
}