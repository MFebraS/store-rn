export const Font = {
	brand: 'Quicksand-Regular',
	light: 'Manjari-Thin',
	regular: 'Manjari-Regular',
	bold: 'Manjari-Bold',
}