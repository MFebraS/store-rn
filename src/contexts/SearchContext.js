import React, { useState, createContext } from 'react';

export const SearchContext = createContext({
	keyword: '',
	setKeyword: ()=>{},
	search: false,
	setSearch: ()=>{},
});

export const SearchContextProvider = props => {
	const [keyword, setKeyword] = useState('');
	const [search, setSearch] = useState(false);

	return (
		<SearchContext.Provider value={{keyword, setKeyword, search, setSearch}}>
			{props.children}
		</SearchContext.Provider>
	);
};
